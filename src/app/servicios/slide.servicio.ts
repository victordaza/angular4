import { Injectable } from '@angular/core';

import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';

import { Ruta } from '../ruta_global';

@Injectable()

export class ServicioSlide{

	/*=============================================
	PETICIONES HTTP PARA TRAER EL ARCHIVO JSON
	=============================================*/

	public url:string;

	constructor(private _http:Http){

		this.url = Ruta.url;	
	
	}

	/*=============================================
   	SELECCIONAR SLIDE
  	=============================================*/ 

	tomarJsonSlide(){

		return this._http.get(this.url+"mostrar-slides").map(resultado => resultado.json())	

	}

	/*=============================================
   	NUEVO SLIDE
  	=============================================*/ 

  	subirImagenSlide(url, items, token, imagen){

  		if(!imagen){

  			return new Promise(function(resolver, rechazar){

  				rechazar("No hay imagen para subir");

  			})
  		
  		}else{

  		   return new Promise(function(resolver, rechazar){

  		   		var formData:any = new FormData();
  		   		var xhr = new XMLHttpRequest();

  		   		formData.append("imagen", imagen[0]);
  		   		formData.append("titulo", items.titulo);
  		   		formData.append("descripcion", items.descripcion);

  		   		xhr.onreadystatechange = function(){

  		   			if(xhr.readyState == 4){

  		   				if(xhr.status == 200){

  		   					resolver(JSON.parse(xhr.response))

  		   				}else{

  		   					rechazar(xhr.response)
  		   				}
  		   			}

  		   		}

  		   		xhr.open("POST", url, true);
  		   		xhr.setRequestHeader("Authorization", token);
  		   		xhr.send(formData);

  		   })

  		}

  	}


  /*=============================================
   ACTUALIZAR SLIDE
  =============================================*/ 

  actualizarItemSlide(url, items, token, imagen){

    if(!imagen){

      return new Promise(function(resolver, rechazar){

         var formData:any = new FormData();
           var xhr = new XMLHttpRequest();

           formData.append("titulo", items.titulo);
           formData.append("descripcion", items.descripcion);
           formData.append("actualizarImagen", 0);
           formData.append("rutaImagenActual", items.imagen);

           xhr.onreadystatechange = function(){

             if(xhr.readyState == 4){

               if(xhr.status == 200){

                 resolver(JSON.parse(xhr.response))

               }else{

                 rechazar(xhr.response)
               }
             }

           }

           xhr.open("PUT", url, true);
           xhr.setRequestHeader("Authorization", token);
           xhr.send(formData);


      })
    
    }else{

       return new Promise(function(resolver, rechazar){

           var formData:any = new FormData();
           var xhr = new XMLHttpRequest();

           formData.append("imagen", imagen[0]);
           formData.append("titulo", items.titulo);
           formData.append("descripcion", items.descripcion);
           formData.append("actualizarImagen", 1);
           formData.append("rutaImagenActual", items.imagen);

           xhr.onreadystatechange = function(){

             if(xhr.readyState == 4){

               if(xhr.status == 200){

                 resolver(JSON.parse(xhr.response))

               }else{

                 rechazar(xhr.response)
               }
             }

           }

           xhr.open("PUT", url, true);
           xhr.setRequestHeader("Authorization", token);
           xhr.send(formData);

       })

    }

  }

  /*=============================================
   BORRAR SLIDE
  =============================================*/ 

  borrarItemSlide(id){

    let headers = new Headers({"Content-Type":"application/json",
                               "Authorization": localStorage.getItem("id")});

    return this._http.delete(this.url+"borrar-slide/"+id, {headers: headers}).map(resultado=> resultado.json())

  }

}