import { Injectable } from '@angular/core';

import { Http, Response, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

import { Observable } from 'rxjs/Observable';

import { Ruta } from '../ruta_global';

@Injectable()

export class ServicioGaleria{

	/*=============================================
	PETICIONES HTTP PARA TRAER EL ARCHIVO JSON
	=============================================*/

	public url:string;

	constructor(private _http:Http){

		this.url = Ruta.url;	
	
	}

	/*=============================================
   	SELECCIONAR GALERÍAS
  	=============================================*/ 

	tomarJsonGaleria(){

		return this._http.get(this.url+"mostrar-fotos").map(resultado => resultado.json())	

	}

	/*=============================================
   	NUEVA FOTO GALERÍA
  	=============================================*/ 

  	subirFotoGaleria(url, token, foto){

  		if(!foto){

  			return new Promise(function(resolver, rechazar){

  				rechazar("No hay imagen para subir");

  			})
  		
  		}else{

  		   return new Promise(function(resolver, rechazar){

  		   		var formData:any = new FormData();
  		   		var xhr = new XMLHttpRequest();

  		   		formData.append("foto", foto[0]);

  		   		xhr.onreadystatechange = function(){

  		   			if(xhr.readyState == 4){

  		   				if(xhr.status == 200){

  		   					resolver(JSON.parse(xhr.response))

  		   				}else{

  		   					rechazar(xhr.response)
  		   				}
  		   			}

  		   		}

  		   		xhr.open("POST", url, true);
  		   		xhr.setRequestHeader("Authorization", token);
  		   		xhr.send(formData);

  		   })

  		}

  	}

  /*=============================================
   BORRAR FOTO
  =============================================*/ 

  borrarItemFoto(id){

    let headers = new Headers({"Content-Type":"application/json",
                               "Authorization": localStorage.getItem("id")});

    return this._http.delete(this.url+"borrar-foto/"+id, {headers: headers}).map(resultado=> resultado.json())

  }


}